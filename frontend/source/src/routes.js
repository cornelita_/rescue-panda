import { useRoutes } from 'react-router-dom'

import expenseRoutes from 'expense/routes'
import arusKasReportRoutes from 'arusKasReport/routes'
import incomeRoutes from 'income/routes'
import staticPageRoutes from 'staticPage/routes'
import activityRoutes from 'activity/routes'
import confirmationRoutes from 'confirmation/routes'
import commonRoutes from 'commons/routes.js'
import userRoutes from 'user/routes'
import roleRoutes from 'role/routes'

const GlobalRoutes = () => {
  const router = useRoutes([
    ...expenseRoutes,
    ...arusKasReportRoutes,
    ...incomeRoutes,
    ...staticPageRoutes,
    ...activityRoutes,
    ...confirmationRoutes,
    ...commonRoutes,
    ...userRoutes,
    ...roleRoutes,
  ])

  return router
}

export default GlobalRoutes
