import React from 'react'
import { Link } from 'react-router-dom'

import { useAuth } from 'commons/auth'
import { Button, TableRow, TableCell } from 'commons/components'

const UserTable = ({ userItem }) => {
  const { isAuth } = useAuth()

  return (
    <TableRow distinct={false}>
      {/* Data Binding User Table Element*/}
      <TableCell>{userItem?.name}</TableCell>
      <TableCell>{userItem?.email}</TableCell>
      <TableCell>{userItem?.allowedPermissions}</TableCell>
      <TableCell>
        <div class="btn-group gap-2">
          {/* View Element Event User Table Element*/}
          <Link to={`/settings/user/${userItem.id}`}>
            <Button variant="tertiary">Detail</Button>
          </Link>
        </div>
      </TableCell>
    </TableRow>
  )
}

export default UserTable
