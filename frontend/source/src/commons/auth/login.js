import React from 'react'
import { Link, Navigate } from 'react-router-dom'
import { AuthConsumer } from '.'
import { Button, InputField } from 'commons/components'

class LoginPage extends React.Component {
  handleLoginWithGoogle = () => {
    this.props.loginGoogle()
  }

  render() {
    if (this.props.isAuth) {
      return <Navigate to="/" />
    }

    return (
      <div className="h-full bg-base-200 grid place-items-center">
        <div className="prose w-full max-w-sm">
          <h1>Log in</h1>
          <div className="card flex-shrink-0 w-full shadow-2xl bg-white">
            <div className="card-body not-prose">
              <InputField
                name="email"
                type="email"
                label="Email"
                placeholder="Masukkan email"
              />
              <InputField
                name="password"
                type="password"
                label="Password"
                placeholder="Masukkan password"
              />
              <div className="flex justify-end">
                <Link
                  to="/forgot-password"
                  className="btn btn-ghost btn-sm normal-case"
                >
                  Lupa Password
                </Link>
              </div>
              <Button variant="primary" className="form-control">
                Masuk
              </Button>
              <div className="text-center text-sm text-neutral/70 mt-1">
                Belum punya akun?{' '}
                <Link to="/register" className="btn-link normal-case">
                  Daftar
                </Link>
              </div>
              <div className="divider">atau</div>
              <Button onClick={this.handleLoginWithGoogle}>
                Masuk dengan Google
              </Button>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const withAuth = props => (
  <AuthConsumer>{values => <LoginPage {...props} {...values} />}</AuthConsumer>
)

export default withAuth
