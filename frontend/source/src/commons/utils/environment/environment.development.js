const development = {
  production: false,
  rootApi: 'http://localhost:7776',
  staticServerApi: 'http://localhost:3003',
}

export default development
