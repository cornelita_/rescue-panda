import { Footer, Hero } from 'commons/components'
import React from 'react'

const LandingPage = () => {
  const DUMMY_BANNER =
    'https://asset-a.grid.id//crop/0x0:0x0/700x465/photo/2022/07/15/panda-3875289_640jpg-20220715103626.jpg'

  return (
    <div className="landing-page">
      <Hero banner={DUMMY_BANNER} />
    </div>
  )
}

export default LandingPage
