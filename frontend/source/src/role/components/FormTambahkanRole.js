import React from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useNavigate } from 'react-router-dom'
import toast from 'react-hot-toast'
import {
  Button,
  Form,
  SelectionField,
  MultiSelectionField,
  InputField,
} from 'commons/components'

import cleanFormData from 'commons/utils/cleanFormData'

import saveRole from '../services/saveRole'

const FormTambahkanRole = ({}) => {
  const { control, handleSubmit } = useForm()

  const navigate = useNavigate()

  const kirim = data => {
    const cleanData = cleanFormData(data)
    saveRole({
      ...cleanData,
    })
      .then(({ data: { data } }) => {
        navigate(`/settings/role`)
      })
      .catch(error => {
        toast.error(
          error.response?.data?.data?.message ||
            error ||
            'Terjadi kesalahan pada sistem. Harap coba lagi!'
        )
      })
  }

  return (
    <Form title="Tambahkan Role" onSubmit={handleSubmit(kirim)}>
      <Controller
        name="name"
        control={control}
        rules={{ required: 'Harap masukkan name' }}
        render={({ field, fieldState }) => (
          <InputField
            label="Name"
            placeholder="Masukkan name"
            fieldState={fieldState}
            {...field}
          />
        )}
      />
      <Controller
        name="allowedPermissions"
        control={control}
        rules={{ required: 'Harap masukkan allowed permissions' }}
        render={({ field, fieldState }) => (
          <InputField
            label="Allowed Permissions"
            placeholder="Masukkan allowed permissions"
            fieldState={fieldState}
            {...field}
          />
        )}
      />
      <div className="card-actions justify-end">
        <Button type="submit" variant="primary">
          Kirim
        </Button>
      </div>
    </Form>
  )
}

export default FormTambahkanRole
